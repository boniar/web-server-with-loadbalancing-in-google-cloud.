resource "google_container_cluster" "mycluster" {
   name = "nginx"
   initial_node_count = 3
   
   node_config {
       machine_type = "g1-small"
       disk_size_gb = 10
   }
}