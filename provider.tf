provider "google" {
  credentials = "${file("./credentials/terraform.json")}"
  project     = "terraform-237713"
  region      = "europe-west1"
  zone        = "europe-west1-b"
}

provider "helm" {
    install_tiller = "true"
    service_account = "tiller"
    namespace = "kube-system"
    kubernetes {
        config_path = "~/.kube/config"
        config_context = "tiller"
    }
}
provider "kubernetes" {
   host     = "${google_container_cluster.mycluster.endpoint}"
}