# Web server with load-balancing in Google cloud.

A small project showing the ability to build and maintain infrastructure as a code.
Used technologies:
- Terraform
- GCP
- Kubernetes
- Helm
- Docker
- Nginx
- Haproxy
